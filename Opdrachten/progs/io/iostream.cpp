#include <iostream>

int main() {
	double d;
	std::cin >> d; // lees d in vanaf het toetsenbord
	std::cout << "d = " << d << '\n'; // druk d af op het scherm en ga naar het begin van de volgende regel
}
