#include <stdio.h>

int main(void) {
	double d;
	scanf("%d", &d); // deze regel bevat $\color{hrred}\textbf{één fout}$!
	printf("d = %lf\n", d);
    return 0;
}
